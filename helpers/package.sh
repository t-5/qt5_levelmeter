#!/bin/bash

cd ..
VERSION=`cat .previous_version`
echo -n "Version (previous version was $VERSION: "
read VERSION

rm -f builds/*.deb

mkdir -p debian/DEBIAN
sudo cat control.in | sed "s#Version: _VERSION_#Version: ${VERSION}#" > debian/DEBIAN/control

mkdir -p debian/usr/lib/python3/dist-packages/qt5_levelmeter
sudo cp -f src/__init__.py debian/usr/lib/python3/dist-packages/qt5_levelmeter

sudo chown -R root.root debian/
sudo chmod -R o+rX debian/
dpkg --build debian && \
    mv debian.deb builds/python3-qt5-levelmeter_${VERSION}_all.deb && \
    aptly repo add t-5 builds && \
    echo -n "$VERSION" > .previous_version

sudo chown -R jh.jh debian/

# make sdist package
rm -f dist/*.tar.gz
cat setup.py|sed "s#version='[0-9.]*'#version='$VERSION'#" > setup.py.new && \
rm -f setup.py && \
mv setup.py.new setup.py && \
python3 setup.py sdist
